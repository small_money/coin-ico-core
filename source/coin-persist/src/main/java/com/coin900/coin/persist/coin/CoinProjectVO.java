package com.coin900.coin.persist.coin;

import com.coin900.coin.persist.FuVO;

import java.util.Date;
import java.util.List;

/**
 * 项目
 *
 * @author shui
 * @create 2018-1-14
 */
public class CoinProjectVO extends FuVO {

    /**
     * token_id
     */
    private Long tokenId;

    /**
     * 项目发布方 id
     */
    private Long releaseUserId;

    /**
     * 名称
     */
    private String name;

    /**
     * 项目简介
     */
    private String introduce;

    /**
     * 图标
     */
    private String logo;

    /**
     * 项目大图
     */
    private String bigImg;

    /**
     * 项目小图
     */
    private String smallImg;

    /**
     * 项目开始时间
     */
    private Date startTime;

    /**
     * 项目截止时间
     */
    private Date closeTime;

    /**
     * 目标金额
     */
    private Long targetAmount;

    /**
     * 已筹金额
     */
    private Long fundedAmount;

    /**
     * 资金进度(已筹金额/总金额)
     */
    private Double amountProgress;

    /**
     * 时间进度(项目进行时间/总募集时间)
     */
    private Double timeProgress;

    /**
     * 中文白皮书
     */
    private String chineseWhritePaper;

    /**
     * 英文白皮书
     */
    private String englishWhritePaper;

    /**
     * 项目详情
     */
    private String detail;

    /**
     * 项目进度，0未发布 1进行中 2即将 3已结束
     */
    private Integer status;

    /**
     * 审核状态，0未通过 1通过 2审核中
     */
    private Integer checkStatus;

    /**
     * 发布状态 0未发布 1已发布
     */
    private Integer publishStatus;

    /***** 非持久化 START *****/

    /**
     * 项目发布方
     */
    private CoinReleaseUserVO releaseUserVO;

    /**
     * 代币
     */
    private CoinTokenVO tokenVO;

    /**
     * 项目支持的虚拟币
     */
    private List<CoinVirtualCoinVO> virtualCoinList;

    /***** 非持久化 END *****/

    public Long getTokenId() {
        return tokenId;
    }

    public void setTokenId(Long tokenId) {
        this.tokenId = tokenId;
    }

    public Long getReleaseUserId() {
        return releaseUserId;
    }

    public void setReleaseUserId(Long releaseUserId) {
        this.releaseUserId = releaseUserId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getBigImg() {
        return bigImg;
    }

    public void setBigImg(String bigImg) {
        this.bigImg = bigImg;
    }

    public String getSmallImg() {
        return smallImg;
    }

    public void setSmallImg(String smallImg) {
        this.smallImg = smallImg;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(Date closeTime) {
        this.closeTime = closeTime;
    }

    public Long getTargetAmount() {
        return targetAmount;
    }

    public void setTargetAmount(Long targetAmount) {
        this.targetAmount = targetAmount;
    }

    public Long getFundedAmount() {
        return fundedAmount;
    }

    public void setFundedAmount(Long fundedAmount) {
        this.fundedAmount = fundedAmount;
    }

    public Double getAmountProgress() {
        return amountProgress;
    }

    public void setAmountProgress(Double amountProgress) {
        this.amountProgress = amountProgress;
    }

    public Double getTimeProgress() {
        return timeProgress;
    }

    public void setTimeProgress(Double timeProgress) {
        this.timeProgress = timeProgress;
    }

    public String getChineseWhritePaper() {
        return chineseWhritePaper;
    }

    public void setChineseWhritePaper(String chineseWhritePaper) {
        this.chineseWhritePaper = chineseWhritePaper;
    }

    public String getEnglishWhritePaper() {
        return englishWhritePaper;
    }

    public void setEnglishWhritePaper(String englishWhritePaper) {
        this.englishWhritePaper = englishWhritePaper;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(Integer checkStatus) {
        this.checkStatus = checkStatus;
    }

    public Integer getPublishStatus() {
        return publishStatus;
    }

    public void setPublishStatus(Integer publishStatus) {
        this.publishStatus = publishStatus;
    }

    public CoinReleaseUserVO getReleaseUserVO() {
        return releaseUserVO;
    }

    public void setReleaseUserVO(CoinReleaseUserVO releaseUserVO) {
        this.releaseUserVO = releaseUserVO;
    }

    public CoinTokenVO getTokenVO() {
        return tokenVO;
    }

    public void setTokenVO(CoinTokenVO tokenVO) {
        this.tokenVO = tokenVO;
    }

    public List<CoinVirtualCoinVO> getVirtualCoinList() {
        return virtualCoinList;
    }

    public void setVirtualCoinList(List<CoinVirtualCoinVO> virtualCoinList) {
        this.virtualCoinList = virtualCoinList;
    }

    @Override
    public String toString() {
        return super.toString() +
                "CoinProjectVO{" +
                "tokenId=" + tokenId +
                ", releaseUserId=" + releaseUserId +
                ", name='" + name + '\'' +
                ", introduce='" + introduce + '\'' +
                ", logo='" + logo + '\'' +
                ", bigImg='" + bigImg + '\'' +
                ", smallImg='" + smallImg + '\'' +
                ", startTime=" + startTime +
                ", closeTime=" + closeTime +
                ", targetAmount=" + targetAmount +
                ", fundedAmount=" + fundedAmount +
                ", amountProgress=" + amountProgress +
                ", timeProgress=" + timeProgress +
                ", chineseWhritePaper='" + chineseWhritePaper + '\'' +
                ", englishWhritePaper='" + englishWhritePaper + '\'' +
                ", detail='" + detail + '\'' +
                ", status=" + status +
                ", checkStatus=" + checkStatus +
                ", publishStatus=" + publishStatus +
                ", releaseUserVO=" + releaseUserVO +
                ", tokenVO=" + tokenVO +
                ", virtualCoinList=" + virtualCoinList +
                '}';
    }
}
package com.coin900.coin.persist.fu;

import com.coin900.coin.persist.FuVO;

/**
 * 资产明细表
 *
 * @author shui
 * @create 2017-12-18
 */
public class FuCapitalDetailVO extends FuVO {

    /**
     * 用户 id
     */
    private Long userId;

    /**
     * 类型 1充值记录 2提现记录
     */
    private Integer type;

    /**
     * 钱包类型 1钱包 2虚拟币
     */
    private Integer walletType;

    /**
     * 目标表的 id
     */
    private Long targetId;

    /**
     * 充值金额
     */
    private Long amount;

    /**
     * 审核状态
     */
    private Integer checkStatus;

    /**
     * 支付方式 1支付宝 2微信 3银行卡 4虚拟币
     */
    private Integer payType;

    /************非持久化数据开始************/

    private String username;

    /**
     * 审核状态名称
     */
    private String checkStatusName;

    /**
     * 支付状态名称
     */
    private String payTypeName;

    /************非持久化数据结束************/

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getTargetId() {
        return targetId;
    }

    public void setTargetId(Long targetId) {
        this.targetId = targetId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCheckStatusName() {
        return checkStatusName;
    }

    public void setCheckStatusName(String checkStatusName) {
        this.checkStatusName = checkStatusName;
    }

    public String getPayTypeName() {
        return payTypeName;
    }

    public void setPayTypeName(String payTypeName) {
        this.payTypeName = payTypeName;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Integer getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(Integer checkStatus) {
        this.checkStatus = checkStatus;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public Integer getWalletType() {
        return walletType;
    }

    public void setWalletType(Integer walletType) {
        this.walletType = walletType;
    }

    @Override
    public String toString() {
        return super.toString() +
                "FuCapitalDetailVO{" +
                "userId=" + userId +
                ", type=" + type +
                ", targetId=" + targetId +
                ", amount=" + amount +
                ", checkStatus=" + checkStatus +
                ", username='" + username + '\'' +
                ", checkStatusName='" + checkStatusName + '\'' +
                ", payTypeName='" + payTypeName + '\'' +
                ", payType='" + payType + '\'' +
                ", walletType='" + walletType + '\'' +
                '}';
    }
}
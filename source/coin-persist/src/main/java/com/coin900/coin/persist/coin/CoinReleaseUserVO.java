package com.coin900.coin.persist.coin;

import com.coin900.coin.persist.FuVO;

/**
 * 项目发布方
 *
 * @author shui
 * @create 2018-1-14
 */
public class CoinReleaseUserVO extends FuVO {

    /**
     * 头像
     */
    private String protrait;

    /**
     * 发布方名称
     */
    private String name;

    /**
     * 联系电话
     */
    private String mobile;

    /**
     * 官网链接
     */
    private String website;

    /**
     * 社交媒体链接
     */
    private String socialMedia;

    public String getProtrait() {
        return protrait;
    }

    public void setProtrait(String protrait) {
        this.protrait = protrait;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getSocialMedia() {
        return socialMedia;
    }

    public void setSocialMedia(String socialMedia) {
        this.socialMedia = socialMedia;
    }

    @Override
    public String toString() {
        return super.toString() +
                "CoinReleaseUserVO{" +
                "protrait='" + protrait + '\'' +
                ", name='" + name + '\'' +
                ", mobile='" + mobile + '\'' +
                ", website='" + website + '\'' +
                ", socialMedia='" + socialMedia + '\'' +
                '}';
    }
}
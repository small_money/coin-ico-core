package com.coin900.coin.persist.sbi;

import com.coin900.coin.persist.FuVO;

/**
 * 省
 */
public class SbiProvinceVO extends FuVO {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return super.toString() +
                "SbiProvinceVO{" +
                "name='" + name + '\'' +
                '}';
    }
}
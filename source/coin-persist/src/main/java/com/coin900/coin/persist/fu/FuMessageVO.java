package com.coin900.coin.persist.fu;

import com.coin900.coin.persist.FuVO;


/**
 * 消息
 *
 * @author shui
 */
public class FuMessageVO extends FuVO {

    /**
     * 类型 1项目
     */
    private Integer type;

    /**
     * 目标 id
     */
    private Long targetId;

    /**
     * 项目进度 1众筹中 2众筹完成 3筹资成功 4筹资失败 5下架
     */
    private Integer projectStatus;

    /**
     * 消息
     */
    private String message;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getTargetId() {
        return targetId;
    }

    public void setTargetId(Long targetId) {
        this.targetId = targetId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getProjectStatus() {
        return projectStatus;
    }

    public void setProjectStatus(Integer projectStatus) {
        this.projectStatus = projectStatus;
    }

    @Override
    public String toString() {
        return super.toString()
                + "FuMessageVO{" +
                "type=" + type +
                ", targetId=" + targetId +
                ", projectStatus='" + projectStatus + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
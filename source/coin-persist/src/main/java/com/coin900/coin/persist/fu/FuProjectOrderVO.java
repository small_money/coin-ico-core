package com.coin900.coin.persist.fu;

import com.coin900.coin.persist.FuVO;

/**
 * 订单
 *
 * @author shui
 */
public class FuProjectOrderVO extends FuVO {

    private Long userId;

    private Long fundId;

    /**
     * 订单编号
     */
    private String orderNo;

    /**
     * 支付方式1微信支付
     */
    private Integer payType;

    /**
     * 订单状态 0未支付 1已支付 3退款 4取消
     */
    private Integer orderStatus;

    /**
     * 订单价格
     */
    private Long price;

    /**
     * 微信预支付交易会话标识
     */
    private String wxPrepayId;

    /**
     * 同步 key
     */
    private Long syncKey;

    /*****非持久化数据开始*****/

    /**
     * 项目标题
     */
    private String title;

    /*****非持久化数据结束*****/

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getFundId() {
        return fundId;
    }

    public void setFundId(Long fundId) {
        this.fundId = fundId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getWxPrepayId() {
        return wxPrepayId;
    }

    public void setWxPrepayId(String wxPrepayId) {
        this.wxPrepayId = wxPrepayId;
    }

    public Long getSyncKey() {
        return syncKey;
    }

    public void setSyncKey(Long syncKey) {
        this.syncKey = syncKey;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return super.toString() +
                "FuProjectOrderVO{" +
                "userId=" + userId +
                ", fundId=" + fundId +
                ", orderNo='" + orderNo + '\'' +
                ", payType=" + payType +
                ", orderStatus=" + orderStatus +
                ", price=" + price +
                ", wxPrepayId='" + wxPrepayId + '\'' +
                ", syncKey=" + syncKey +
                ", title=" + title +
                '}';
    }

}
package com.coin900.coin.persist.fu;

import com.coin900.coin.persist.FuVO;

/**
 * @author shui
 */
public class FuMessageUserVO extends FuVO {

    private Long userId;

    private Long messageId;

    /**
     * 0未读 1已读
     */
    private Integer status;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return super.toString() +
                "FuMessageUserVO{" +
                "userId=" + userId +
                ", messageId=" + messageId +
                ", status=" + status +
                '}';
    }
}
package com.coin900.coin.persist.fu;

import com.coin900.coin.persist.FuVO;


/**
 * 项目关注表
 *
 * @author shui
 */
public class FuProjectFollowVO extends FuVO {

    private Long userId;

    private Long projectId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return super.toString() +
                "FuProjectFollowVO{" +
                "userId=" + userId +
                ", projectId=" + projectId +
                '}';
    }
}
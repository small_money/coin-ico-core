package com.coin900.coin.persist.sop;

import com.coin900.coin.persist.FuVO;

/**
* VO 实体类
* created by shui
*/
public class SopRoleButtonVO  extends FuVO {


    private Long roleId;

    private Long buttonId;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId){
        this.roleId = roleId;
    }

    public Long getButtonId() {
        return buttonId;
    }

    public void setButtonId(Long buttonId){
        this.buttonId = buttonId;
    }

}
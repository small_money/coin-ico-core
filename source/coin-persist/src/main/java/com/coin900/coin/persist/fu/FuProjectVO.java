package com.coin900.coin.persist.fu;

import com.coin900.coin.persist.FuVO;

import java.util.Date;
import java.util.List;

/**
 * 众筹项目
 *
 * @author shui
 */
public class FuProjectVO extends FuVO {

    /**
     * 分类 id
     */
    private Long categoryId;

    /**
     * 项目主图
     */
    private String projectImg;

    /**
     * 标题
     */
    private String title;

    /**
     * 项目进度，1众筹中 2众筹完成 3筹资成功 4筹资失败 5下架
     */
    private Integer status;

    /**
     * 目标金额
     */
    private Long targetAmount;

    /**
     * 基准金额
     */
    private Long baseAmount;

    /**
     * 已筹金额
     */
    private Long fundedAmount;

    /**
     * 总意向金额
     */
    private Long intentionAmount;

    /**
     * 所属区域 （城市）
     */
    private Long cityId;

    /**
     * 审核是否通过，0未通过 1通过 2审核中
     */
    private Integer checkStatus;

    /**
     * 认筹支付方式 1余额 4虚拟币
     */
    private Integer payType;

    /**
     * 项目截止时间
     */
    private Date closeTime;

    /**
     * 项目是否发布 0未发布 1已发布
     */
    private Integer isPublish;

    /**
     * 认筹人数
     */
    private Long fundNumber;

    /**
     * 关注人数
     */
    private Long followNumber;

    /**
     * 总意向人数
     */
    private Long intentionNumber;

    /**
     * 联系人姓名
     */
    private String linkmanName;

    /**
     * 联系人电话
     */
    private String linkmanMobile;

    /**
     * 项目详情
     */
    private String detail;

    /**
     * 项目简介
     */
    private String introduce;

    private Long provinceId;

    /**
     * ========== 非持久化数据 start ==========
     */


    /**
     * 项目标签
     */
    private List<FuLabelVO> labelVOList;

    /**
     * 项目进度名称 1众筹中 2众筹完成 3筹资成功 4筹资失败 5下架
     */
    private String statusName;

    /**
     * 剩余天数
     */
    private String remainDay;

    /**
     * 城市名称
     */
    private String cityName;

    /**
     * 进度 100%
     */
    private String statusNumber;

    /**
     * 用户认筹金额
     */
    private Long fundAmount;

    /**
     * 项目分类名称
     */
    private String categoryName;

    private String provinceName;


    /**
     * ========== 非持久化数据 end ==========
     */

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getProjectImg() {
        return projectImg;
    }

    public void setProjectImg(String projectImg) {
        this.projectImg = projectImg;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getTargetAmount() {
        return targetAmount;
    }

    public void setTargetAmount(Long targetAmount) {
        this.targetAmount = targetAmount;
    }

    public Long getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(Long baseAmount) {
        this.baseAmount = baseAmount;
    }

    public Long getFundedAmount() {
        return fundedAmount;
    }

    public void setFundedAmount(Long fundedAmount) {
        this.fundedAmount = fundedAmount;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Integer getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(Integer checkStatus) {
        this.checkStatus = checkStatus;
    }

    public Date getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(Date closeTime) {
        this.closeTime = closeTime;
    }

    public Integer getIsPublish() {
        return isPublish;
    }

    public void setIsPublish(Integer isPublish) {
        this.isPublish = isPublish;
    }

    public Long getFundNumber() {
        return fundNumber;
    }

    public void setFundNumber(Long fundNumber) {
        this.fundNumber = fundNumber;
    }

    public Long getFollowNumber() {
        return followNumber;
    }

    public void setFollowNumber(Long followNumber) {
        this.followNumber = followNumber;
    }

    public String getLinkmanName() {
        return linkmanName;
    }

    public void setLinkmanName(String linkmanName) {
        this.linkmanName = linkmanName;
    }

    public String getLinkmanMobile() {
        return linkmanMobile;
    }

    public void setLinkmanMobile(String linkmanMobile) {
        this.linkmanMobile = linkmanMobile;
    }

    public List<FuLabelVO> getLabelVOList() {
        return labelVOList;
    }

    public void setLabelVOList(List<FuLabelVO> labelVOList) {
        this.labelVOList = labelVOList;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getRemainDay() {
        return remainDay;
    }

    public void setRemainDay(String remainDay) {
        this.remainDay = remainDay;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getStatusNumber() {
        return statusNumber;
    }

    public void setStatusNumber(String statusNumber) {
        this.statusNumber = statusNumber;
    }

    public Long getFundAmount() {
        return fundAmount;
    }

    public void setFundAmount(Long fundAmount) {
        this.fundAmount = fundAmount;
    }

    public Long getIntentionAmount() {
        return intentionAmount;
    }

    public void setIntentionAmount(Long intentionAmount) {
        this.intentionAmount = intentionAmount;
    }

    public Long getIntentionNumber() {
        return intentionNumber;
    }

    public void setIntentionNumber(Long intentionNumber) {
        this.intentionNumber = intentionNumber;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Long getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }


    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    @Override
    public String toString() {
        return super.toString() +
                "FuProjectVO{" +
                "categoryId=" + categoryId +
                ", projectImg='" + projectImg + '\'' +
                ", title='" + title + '\'' +
                ", status=" + status +
                ", targetAmount=" + targetAmount +
                ", baseAmount=" + baseAmount +
                ", fundedAmount=" + fundedAmount +
                ", cityId=" + cityId +
                ", checkStatus=" + checkStatus +
                ", closeTime=" + closeTime +
                ", isPublish=" + isPublish +
                ", fundNumber=" + fundNumber +
                ", followNumber=" + followNumber +
                ", linkmanName='" + linkmanName + '\'' +
                ", linkmanMobile='" + linkmanMobile + '\'' +
                ", detail='" + detail + '\'' +
                ", labelVOList=" + labelVOList +
                ", statusName='" + statusName + '\'' +
                ", remainDay=" + remainDay +
                ", cityName='" + cityName + '\'' +
                ", statusNumber='" + statusNumber + '\'' +
                ", fundAmount=" + fundAmount +
                ", introduce=" + introduce +
                ", categoryName=" + categoryName +
                ", payType=" + payType +
                '}';
    }
}
package com.coin900.coin.persist.sbi;

import com.coin900.coin.persist.FuVO;


/**
 * 区
 *
 * @author shui
 */
public class SbiDistrictVO extends FuVO {

    private String name;

    private Long cityId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    @Override
    public String toString() {
        return super.toString() +
                "SbiDistrictVO{" +
                "name='" + name + '\'' +
                ", cityId=" + cityId +
                '}';
    }
}
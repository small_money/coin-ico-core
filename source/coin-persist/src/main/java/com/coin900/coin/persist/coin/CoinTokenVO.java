package com.coin900.coin.persist.coin;

import com.coin900.coin.persist.FuVO;

/**
 * 代币
 *
 * @author shui
 * @create 2018-1-14
 */
public class CoinTokenVO extends FuVO {

    /**
     * 代币名称
     */
    private String name;

    /**
     * 代币简称
     */
    private String shortName;

    /**
     * 代币总量
     */
    private Long totalNumber;

    /**
     * ICO 总量
     */
    private Long icoNumber;

    /**
     * 最低目标
     */
    private Long lowestNumber;

    /**
     * 最高目标
     */
    private Long highestNumber;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public Long getTotalNumber() {
        return totalNumber;
    }

    public void setTotalNumber(Long totalNumber) {
        this.totalNumber = totalNumber;
    }

    public Long getIcoNumber() {
        return icoNumber;
    }

    public void setIcoNumber(Long icoNumber) {
        this.icoNumber = icoNumber;
    }

    public Long getLowestNumber() {
        return lowestNumber;
    }

    public void setLowestNumber(Long lowestNumber) {
        this.lowestNumber = lowestNumber;
    }

    public Long getHighestNumber() {
        return highestNumber;
    }

    public void setHighestNumber(Long highestNumber) {
        this.highestNumber = highestNumber;
    }

    @Override
    public String toString() {
        return super.toString() +
                "CoinTokenVO{" +
                "name='" + name + '\'' +
                ", shortName='" + shortName + '\'' +
                ", totalNumber=" + totalNumber +
                ", icoNumber=" + icoNumber +
                ", lowestNumber=" + lowestNumber +
                ", highestNumber=" + highestNumber +
                '}';
    }
}
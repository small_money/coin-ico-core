package com.coin900.coin.persist.sop;

import com.coin900.coin.persist.FuVO;

import java.util.List;

/**
 * VO 实体类
 * created by shui
 */
public class SopRoleVO extends FuVO {

    /**
     * 角色名称
     */
    private String roleName;

    /***** 非持久化数据 START *****/

    /**
     * 角色的菜单列表
     */
    private List<SopMenuVO> sopMenuList;

    /**
     * 角色的按钮列表
     */
    private List<SopButtonVO> sopButtonList;

    /***** 非持久化数据 END *****/

    public String getRoleName() {
        return roleName;
    }

    public List<SopMenuVO> getSopMenuList() {
        return sopMenuList;
    }

    public void setSopMenuList(List<SopMenuVO> sopMenuList) {
        this.sopMenuList = sopMenuList;
    }

    public List<SopButtonVO> getSopButtonList() {
        return sopButtonList;
    }

    public void setSopButtonList(List<SopButtonVO> sopButtonList) {
        this.sopButtonList = sopButtonList;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @Override
    public String toString() {
        return super.toString() +
                "SopRoleVO{" +
                "roleName='" + roleName + '\'' +
                ", sopMenuList=" + sopMenuList +
                ", sopButtonList=" + sopButtonList +
                '}';
    }
}
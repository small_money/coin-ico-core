package com.coin900.coin.persist.fu;

import com.coin900.coin.persist.FuVO;

/**
 * 角色表
 *
 * @author shui
 */
public class FuRoleVO extends FuVO {

    private String roleName;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @Override
    public String toString() {
        return super.toString() +
                "FuRoleVO{" +
                "roleName='" + roleName + '\'' +
                '}';
    }
}
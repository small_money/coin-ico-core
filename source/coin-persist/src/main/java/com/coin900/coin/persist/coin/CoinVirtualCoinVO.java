package com.coin900.coin.persist.coin;

import com.coin900.coin.persist.FuVO;

/**
 * 虚拟币
 *
 * @author shui
 * @create 2018-1-14
 */
public class CoinVirtualCoinVO extends FuVO {

    /**
     * 虚拟币名称
     */
    private String coinName;

    public String getCoinName() {
        return coinName;
    }

    public void setCoinName(String coinName) {
        this.coinName = coinName;
    }

}
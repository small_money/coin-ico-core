package com.coin900.coin.base;

/**
 * @author shui
 * @create 2018-01-15
 **/
public class CoinConstants {

    /**
     * 项目状态，0未发布 1进行中 2即将 3已结束
     */
    public static final int COIN_PROJECT_STATUS_UNPUBLISHED = 0;
    public static final int COIN_PROJECT_STATUS_ING = 1;
    public static final int COIN_PROJECT_STATUS_WAITING = 2;
    public static final int COIN_PROJECT_STATUS_FINISHED = 3;

    /**
     * 审核状态 0未通过 1通过 2审核中
     */
    public static final int COIN_CHECK_STATUS_FAIL = 0;
    public static final int COIN_CHECK_STATUS_SUCCESS = 1;
    public static final int COIN_CHECK_STATUS_ING = 2;

    /**
     * 发布状态 0未发布 1已发布
     */
    public static final int COIN_PUBLISH_STATUS_UNPUBLICHED = 0;
    public static final int COIN_PUBLISH_STATUS_PUBLICHED = 1;
}

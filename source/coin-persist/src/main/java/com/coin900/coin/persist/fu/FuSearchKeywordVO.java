package com.coin900.coin.persist.fu;

import com.coin900.coin.persist.FuVO;

/**
 * 搜索关键字记录
 *
 * @author shui
 */
public class FuSearchKeywordVO extends FuVO {

    private String keyword;

    private Long searchNumber;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Long getSearchNumber() {
        return searchNumber;
    }

    public void setSearchNumber(Long searchNumber) {
        this.searchNumber = searchNumber;
    }

    @Override
    public String toString() {
        return super.toString()
                + "FuSearchKeywordVO{" +
                "keyword='" + keyword + '\'' +
                ", searchNumber=" + searchNumber +
                '}';
    }
}
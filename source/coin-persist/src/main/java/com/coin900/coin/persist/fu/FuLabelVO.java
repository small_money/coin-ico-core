package com.coin900.coin.persist.fu;

import com.coin900.coin.persist.FuVO;


/**
 * 标签
 *
 * @author shui
 */
public class FuLabelVO extends FuVO {

    /**
     * 标签名
     */
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return super.toString()
                + "FuLabelVO{" +
                "name='" + name + '\'' +
                '}';
    }
}
package com.coin900.coin.persist.fu;

import com.coin900.coin.persist.FuVO;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * 七牛文件
 *
 * @author shui
 * @create 2017-11-23
 */
public class FuFileVO extends FuVO {

    /**
     * 七牛的appid和bucket
     */
    private String qiniuAppBucket;

    @NotEmpty(message = "hash 不能为空")
    private String hash;

    @NotEmpty(message = "md5 不能为空")
    private String md5;

    /**
     * 文件类型值
     */
    @NotEmpty(message = "文件不能为空")
    private String fileType;

    /**
     * 文件大小单位字节
     */
    private Long fileSize;

    /**
     * 文件路径
     */
    @NotEmpty(message = "文件路径不能为空")
    private String filePath;

    public String getQiniuAppBucket() {
        return qiniuAppBucket;
    }

    public void setQiniuAppBucket(String qiniuAppBucket) {
        this.qiniuAppBucket = qiniuAppBucket;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public String toString() {
        return toString() +
                "FuFileVO{" +
                "qiniuAppBucket='" + qiniuAppBucket + '\'' +
                ", hash='" + hash + '\'' +
                ", md5='" + md5 + '\'' +
                ", fileType='" + fileType + '\'' +
                ", fileSize=" + fileSize +
                ", filePath='" + filePath + '\'' +
                '}';
    }
}
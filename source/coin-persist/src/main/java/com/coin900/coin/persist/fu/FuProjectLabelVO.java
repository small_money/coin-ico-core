package com.coin900.coin.persist.fu;

import com.coin900.coin.persist.FuVO;


/**
 * 项目标签表
 *
 * @author shui
 */
public class FuProjectLabelVO extends FuVO {

    private Long projectId;

    private Long labelId;

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getLabelId() {
        return labelId;
    }

    public void setLabelId(Long labelId) {
        this.labelId = labelId;
    }

    @Override
    public String toString() {
        return super.toString()
                + "FuProjectLabelVO{" +
                "projectId=" + projectId +
                ", labelId=" + labelId +
                '}';
    }
}
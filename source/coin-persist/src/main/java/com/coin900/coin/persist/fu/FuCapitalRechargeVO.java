package com.coin900.coin.persist.fu;

import java.util.Date;

/**
 * 资产充值
 *
 * @author shui
 * @create 2017-12-18
 */
public class FuCapitalRechargeVO extends FuCapitalDetailVO {

    /**
     * 审核时间
     */
    private Date checkTime;

    public Date getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    @Override
    public String toString() {
        return super.toString() +
                "FuCapitalRechargeVO{" +
                ", checkTime=" + checkTime +
                '}';
    }
}
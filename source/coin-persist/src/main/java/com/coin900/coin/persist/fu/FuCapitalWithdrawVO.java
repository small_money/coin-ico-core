package com.coin900.coin.persist.fu;

import java.util.Date;

/**
 * 资产提现
 *
 * @author shui
 * @create 2017-12-18
 */
public class FuCapitalWithdrawVO extends FuCapitalDetailVO {

    /**
     * 审核时间
     */
    private Date checkTime;

    /**
     * 到账时间
     */
    private Date withdrawTime;

    public Date getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    public Date getWithdrawTime() {
        return withdrawTime;
    }

    public void setWithdrawTime(Date withdrawTime) {
        this.withdrawTime = withdrawTime;
    }

    @Override
    public String toString() {
        return super.toString() +
                "FuCapitalWithdrawVO{" +
                ", checkTime=" + checkTime +
                ", withdrawTime=" + withdrawTime +
                '}';
    }
}
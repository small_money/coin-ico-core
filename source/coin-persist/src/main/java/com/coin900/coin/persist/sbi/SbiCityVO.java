package com.coin900.coin.persist.sbi;

import com.coin900.coin.persist.FuVO;


/**
 * 市
 *
 * @author shui
 */
public class SbiCityVO extends FuVO {

    private String name;

    private Long provinceId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }

    @Override
    public String toString() {
        return super.toString() +
                "SbiCityVO{" +
                "name='" + name + '\'' +
                ", provinceId=" + provinceId +
                '}';
    }
}
package com.coin900.coin.persist;

import com.coin900.coin.persist.fu.FuUserVO;

import java.io.Serializable;
import java.util.Date;

/**
 * @author shui
 * @create 2017-11-20
 **/
public class FuVO implements Serializable {

    private Long id;
    private Integer dataStatus;
    private Date createdDate;
    private Date modifiedDate;
    private Long createdBy;
    private String createdName;
    private Long modifiedBy;
    private String modifiedName;

    public void setCMInfor(FuUserVO userVO) {
        Date now = new Date();
        this.setDataStatus(1);
        this.setCreatedDate(now);
        this.setCreatedBy(userVO.getId());
        this.setModifiedDate(now);
        this.setModifiedBy(userVO.getId());
    }

    public void setModifiedInfor(FuUserVO userVO) {
        Date now = new Date();
        this.setDataStatus(1);
        this.setModifiedDate(now);
        this.setModifiedBy(userVO.getId());
    }

    public void setCreatedInfor(FuUserVO userVO) {
        Date now = new Date();
        this.setDataStatus(1);
        this.setCreatedDate(now);
        this.setCreatedBy(userVO.getId());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getDataStatus() {
        return dataStatus;
    }

    public void setDataStatus(Integer dataStatus) {
        this.dataStatus = dataStatus;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedName() {
        return createdName;
    }

    public void setCreatedName(String createdName) {
        this.createdName = createdName;
    }

    public Long getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Long modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getModifiedName() {
        return modifiedName;
    }

    public void setModifiedName(String modifiedName) {
        this.modifiedName = modifiedName;
    }

    @Override
    public String toString() {
        return "FuVO{" +
                "id=" + id +
                ", dataStatus=" + dataStatus +
                ", createdDate=" + createdDate +
                ", modifiedDate=" + modifiedDate +
                ", createdBy=" + createdBy +
                ", createdName='" + createdName + '\'' +
                ", modifiedBy=" + modifiedBy +
                ", modifiedName='" + modifiedName + '\'' +
                '}';
    }
}
